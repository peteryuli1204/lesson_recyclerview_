package com.example.poke_a_monster;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Move> moves = new ArrayList<>();
        moves.add(new Move("Electric bolt", "30/30 pp", R.drawable.electric));
        moves.add(new Move("Electric shock", "40/40 pp", R.drawable.electric));
        moves.add(new Move("Fast attack", "30/30 pp", R.drawable.normal));
        moves.add(new Move("Sunder", "10/10 pp", R.drawable.electric));
        MovesAdapter movesAdapter = new MovesAdapter(moves); // Creates adapter with data

        RecyclerView movesRecyclerView = findViewById(R.id.moves); // Grabbing the "view"
        movesRecyclerView.setLayoutManager(new LinearLayoutManager(this)); // recyclerView needs this.
        movesRecyclerView.setAdapter(movesAdapter); // Connects adapter with recyclerView
    }
}