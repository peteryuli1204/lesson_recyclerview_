package com.example.poke_a_monster;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MovesAdapter extends RecyclerView.Adapter<MovesAdapter.ViewHolder> {
    private ArrayList<Move> moves;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public Button button;
        public TextView pp;
        public ImageView image;

        public ViewHolder(View view) {
            super(view); // <- view is R.layout.move
            this.button = view.findViewById(R.id.move_button);
            this.pp = view.findViewById(R.id.pp);
            this.image = view.findViewById(R.id.type_image);
        }
    }

    public MovesAdapter(ArrayList<Move> moves) {
        this.moves = moves;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get R.layout.move <- Create a "move.xml" view
        // R -> res (directory)
        // layout -> /layout (directory)
        // R.layout.move -> res/layout/move.xml
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.move, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // holder is the view that you want to give data to.
        // Replace the button text
        // when the program wants to fill index #0, give you position = 0
        // when the program wants to fill index #1, give you position = 1
        holder.button.setText(moves.get(position).name);
        holder.pp.setText(moves.get(position).pp);
        holder.image.setImageResource(moves.get(position).image);
    }

    @Override
    public int getItemCount() {
        // RecyclerView will cycle through this whole size.
        return moves.size();
    }
}